CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 
INTRODUCTION
------------
Based on Drupal core's Text module, AES Encrypted Text provides encrypted
storage for several text field types for the Field module. Currently supported
types include text, long text, and long text and summary.
 * For a full description of the module, visit the project page:
   https://drupal.org/project/aes_text
 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/aes_text

REQUIREMENTS
------------
This module requires the following modules:
 * AES (https://www.drupal.org/project/aes)

INSTALLATION
------------
Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
-------------
No configuration options are available for AES Encrypted Text.
